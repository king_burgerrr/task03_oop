package com.veselskyi;

public class chemestry {
    private String ForFloor;
    private String ForTile;
    private String ForCloth;
    private String ForWood;
    private String ForMetal;
    private String ForTechnic;

    public chemestry(String forFloor, String forTile, String forCloth, String forWood, String forMetal, String forTechnic) {
        ForFloor = forFloor;
        ForTile = forTile;
        ForCloth = forCloth;
        ForWood = forWood;
        ForMetal = forMetal;
        ForTechnic = forTechnic;
    }

    public String getForFloor() {
        return ForFloor;
    }

    public void setForFloor(String forFloor) {
        ForFloor = forFloor;
    }

    public String getForTile() {
        return ForTile;
    }

    public void setForTile(String forTile) {
        ForTile = forTile;
    }

    public String getForCloth() {
        return ForCloth;
    }

    public void setForCloth(String forCloth) {
        ForCloth = forCloth;
    }

    public String getForWood() {
        return ForWood;
    }

    public void setForWood(String forWood) {
        ForWood = forWood;
    }

    public String getForMetal() {
        return ForMetal;
    }

    public void setForMetal(String forMetal) {
        ForMetal = forMetal;
    }

    public String getForTechnic() {
        return ForTechnic;
    }

    public void setForTechnic(String forTechnic) {
        ForTechnic = forTechnic;
    }
}
