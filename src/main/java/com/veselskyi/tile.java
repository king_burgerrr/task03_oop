package com.veselskyi;

public class tile {
    private String title;
    private String count;
    private String price;

    public tile(String title, String count, String price) {
        this.title = title;
        this.count = count;
        this.price = price;
    }

    @Override
    public String toString() {
        return "tile{" +
                "title='" + title + '\'' +
                ", count='" + count + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
