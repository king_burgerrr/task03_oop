package com.veselskyi;

public class technics extends chemestry {
    private String title;
    private String count;
    private String price;

    public technics(String forFloor, String forTile, String forCloth, String forWood, String forMetal, String forTechnic, String title, String count, String price) {
        super(forFloor, forTile, forCloth, forWood, forMetal, forTechnic);
        this.title = title;
        this.count = count;
        this.price = price;
    }

    @Override
    public String toString() {
        return "technics{" +
                "title='" + title + '\'' +
                ", count='" + count + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
