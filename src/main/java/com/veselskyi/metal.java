package com.veselskyi;

public class metal extends chemestry{
    private String title;
    private String count;
    private String price;
    private String comment;

    public metal(String forFloor, String forTile, String forCloth, String forWood, String forMetal, String forTechnic, String title, String count, String price, String comment) {
        super(forFloor, forTile, forCloth, forWood, forMetal, forTechnic);
        this.title = title;
        this.count = count;
        this.price = price;
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "metal{" +
                "title='" + title + '\'' +
                ", count='" + count + '\'' +
                ", price='" + price + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
