package com.veselskyi;

public class cloth extends chemestry {
    private String title;
    private String weight;
    private String count;
    private String price;

    public cloth(String forFloor, String forTile, String forCloth, String forWood, String forMetal, String forTechnic, String title, String weight, String count, String price) {
        super(forFloor, forTile, forCloth, forWood, forMetal, forTechnic);
        this.title = title;
        this.weight = weight;
        this.count = count;
        this.price = price;
    }

    @Override
    public String toString() {
        return "cloth{" +
                "title='" + title + '\'' +
                ", weight='" + weight + '\'' +
                ", count='" + count + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
